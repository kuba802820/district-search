f = open('data.txt','r')
temp = []
f1 = open('out.txt','w')
for line in f.readlines():
    if(len(line)>0):
        [x1, y1, z1, x2, y2, z2 ,name] = line.replace('{','').replace('},','').split(',')
        temp.append({'x1': int(x1)+3000,
            'y1': int(y1[1:])+3000,
            'x2': int(x2[1:])+3000,
            'y2': int(y2[1:])+3000,
            'description': 'opis',
            'name': name.replace('\n','').replace('"','')[1:]})
        
removedDups = [dict(t) for t in {tuple(d.items()) for d in temp}]
for item in removedDups:
    # print(item)
    f1.write(str(item)+","+'\n')
# f1.close()
f.close()
# print(temp)